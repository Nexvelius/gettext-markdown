- foo
  - item 1
  - item 2
  - item 3

## head

- foo

- bar
  - item 4
  - item 5
  - item 6

## head 2

1. foo
  - item 7
  - item 8
  - item 9
2. bar
  1. item 10
  2. item 11
  3. item 12
