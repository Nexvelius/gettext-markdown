'use strict';

const gmd = require('../libs/gettext-markdown.js');
const Glob = require('glob').Glob;
const path = require('path');

describe('gettext-markdown', function() {
  it('validation', function(done) {
    let fns = [];
    let g = new Glob(path.join(__dirname, 'fixtures', '*.md'), {nodir: true})
        .on('match', (fn) => {
          fns.push(fn);
        })
        .on('end', () => {
          let result = [];
          for (let i in fns) {
            let fn = fns[i];
            gmd.validate(fn)
              .then((r) => {
                result.push(null);
                assert.equal(r, true);
              }, (r) => {
                result.push(r);
              });
          }
          let f = () => {
            if (result.length === fns.length) {
              done(result.filter((x) => x != undefined)[0]);
            } else {
              setTimeout(f, 1);
            }
          };
          f();
        });
  });
});
